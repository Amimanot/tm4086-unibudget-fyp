package com.ftsm.tm4086.a162021.unibudget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class BudgetAdapter extends RecyclerView.Adapter<BudgetAdapter.ViewHolder> {

    List<Budget> list_budget;
    SimpleDateFormat sdf = new SimpleDateFormat("E MMM dd HH:mm");
    DecimalFormat df = new DecimalFormat("#.00");

    public BudgetAdapter(List<Budget> budget) {
        list_budget = budget;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView textview_note, textview_value, textview_timestamp, textview_frequency;

        public ViewHolder(View itemView){
            super(itemView);
            textview_note = itemView.findViewById(R.id.recyclerview_textview_note);
            textview_value = itemView.findViewById(R.id.recyclerview_textview_value);
            textview_timestamp = itemView.findViewById(R.id.recyclerview_textview_timestamp);
            //textview_frequency = itemView.findViewById(R.id.recyclerview_textview_frequency);

        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View budgetView = inflater.inflate(R.layout.layout_recyclerview, parent, false);

        // Return a new holder instance
        return new ViewHolder(budgetView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Budget budget = list_budget.get(position);

        TextView note = holder.textview_note;
        TextView value = holder.textview_value;
        //TextView frequency = holder.textview_frequency;
        TextView timestamp = holder.textview_timestamp;
        note.setText(budget.getNote());
        value.setText("RM " + df.format(budget.getValue()));
        //frequency.setText(budget.getFrequency());
        timestamp.setText(sdf.format(budget.getTimestamp()));
    }

    @Override
    public int getItemCount() {
        return list_budget.size();
    }
}
