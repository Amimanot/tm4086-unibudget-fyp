package com.ftsm.tm4086.a162021.unibudget;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {

    Button button_logout, button_update, button_add_random_data;
    EditText editText_daily_limit, editText_monthly_limit, editText_semester_limit;
    FirebaseFirestore db;
    FirebaseAuth auth;
    FirebaseUser user;
    String userID, daily, monthly, semester;
    DocumentReference docRef;
    DocumentSnapshot document;

    final String TAG = "FireStoreUpdateData";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setTitle(getString(R.string.settings));

        button_logout = findViewById(R.id.settings_button_logout);
        button_update = findViewById(R.id.settings_button_update);
        button_add_random_data = findViewById(R.id.settings_add_random_data);
        editText_daily_limit = findViewById(R.id.settings_edittext_daily);
        editText_monthly_limit = findViewById(R.id.settings_edittext_monthly);
        editText_semester_limit = findViewById(R.id.settings_edittext_semetster);

        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        assert user != null;
        userID = user.getUid();

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        docRef = db.collection("users").document(userID);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    document = task.getResult();
                    assert document != null;
                    if (document.exists()) {
                        daily = Objects.requireNonNull(document.get("dailylimit")).toString();
                        monthly = Objects.requireNonNull(document.get("monthlylimit")).toString();
                        semester = Objects.requireNonNull(document.get("semesterlimit")).toString();
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        editText_daily_limit.setText(daily);
                        editText_monthly_limit.setText(monthly);
                        editText_semester_limit.setText(semester);

                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });

        button_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    final Map<String, Object> limit = new HashMap<>();
                    limit.put("dailylimit", editText_daily_limit.getText().toString());
                    limit.put("monthlylimit", editText_monthly_limit.getText().toString());
                    limit.put("semesterlimit", editText_semester_limit.getText().toString());

                    if (TextUtils.isEmpty(editText_daily_limit.getText().toString()) || TextUtils.isEmpty(editText_monthly_limit.getText().toString()) || TextUtils.isEmpty(editText_semester_limit.getText().toString())) {
                        Toast.makeText(SettingsActivity.this, getString(R.string.settings_empty_field), Toast.LENGTH_LONG).show();
                    } else{
                        db.collection("users").document(userID)
                                .update(limit)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "DocumentSnapshot successfully updated!");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.w(TAG, "Error updating document", e);
                                    }
                                });

                        Toast.makeText(SettingsActivity.this, getString(R.string.update_limit_data_message), Toast.LENGTH_LONG).show();
                        finish();
                    }
                }catch (Exception e){
                    Log.w(TAG, e);
                    Toast.makeText(SettingsActivity.this, getString(R.string.settings_empty_field), Toast.LENGTH_LONG).show();
                }
            }
        });

        button_add_random_data.setVisibility(View.GONE);

        button_add_random_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AddRandomData().addRandomData(userID);
                Toast.makeText(SettingsActivity.this, getString(R.string.add_random_data_message), Toast.LENGTH_LONG).show();
                finish();
            }
        });

        button_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder logout_alert_dialog_builder = new AlertDialog.Builder(SettingsActivity.this);

                logout_alert_dialog_builder.setMessage(getString(R.string.logout) + "?")
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent login_intent = new Intent(SettingsActivity.this, LoginActivity.class);
                                auth.signOut();
                                login_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(login_intent);
                                finish();
                            }
                        }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                logout_alert_dialog_builder.show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}