package com.ftsm.tm4086.a162021.unibudget;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddDataActivity extends AppCompatActivity {

    Button button_submit;
    EditText editText_date, editText_time, editText_note, editText_value;
    FirebaseFirestore db;
    FirebaseAuth auth;
    FirebaseUser user;
    //Spinner spinner_type;
    Calendar calendar;
    int hour, minute, day, month, year, actual_month;
    String userID, type, date_string;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_data);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        assert user != null;
        userID = user.getUid();
        final String TAG = "FireStoreAddData";

        calendar = Calendar.getInstance();
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        actual_month = month + 1;
        year = calendar.get(Calendar.YEAR);
        button_submit = findViewById(R.id.add_button_submit);
        editText_date = findViewById(R.id.add_edittext_date);
        editText_time = findViewById(R.id.add_edittext_time);
        editText_note = findViewById(R.id.add_edittext_note);
        editText_value = findViewById(R.id.add_edittext_value);
        //spinner_type = findViewById(R.id.add_spinner_type);

        editText_date.setText(day + "/" + actual_month + "/" + year);
        editText_time.setText(String.format("%02d:%02d", hour, minute));

        assert type != null;
        if (type.equals("income")){
            setTitle(getString(R.string.add) + " " + getString(R.string.income));
        }else if (type.equals("expense")){
            setTitle(getString(R.string.add) + " " + getString(R.string.expense));
        }

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        editText_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(AddDataActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year_edit, int month_edit, int day_edit) {
                        int real_month_edit = month_edit + 1;
                        editText_date.setText(day_edit + "/" + real_month_edit + "/" + year_edit);
                    }
                }, year, month, day).show();
            }
        });

        editText_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TimePickerDialog(AddDataActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour_edit, int minute_edit) {
                        editText_time.setText(String.format("%02d:%02d", hour_edit, minute_edit));
                    }
                }, hour, minute, true).show();
            }
        });

        button_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                date_string = editText_date.getText().toString() + " " + editText_time.getText().toString();
                SimpleDateFormat date_format = new SimpleDateFormat("d/M/yyyy H:m");
                try {
                    date = date_format.parse(date_string);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (TextUtils.isEmpty(editText_note.getText().toString()) || TextUtils.isEmpty(editText_value.getText().toString()))
                {
                    Toast.makeText(AddDataActivity.this, getString(R.string.settings_empty_field), Toast.LENGTH_LONG).show();
                }else {
                    final Map<String, Object> data = new HashMap<>();
                    data.put("value", editText_value.getText().toString());
                    data.put("notes", editText_note.getText().toString());
                    assert date != null;
                    data.put("timestamp", new Timestamp(date));

                    /*switch ((int) spinner_type.getSelectedItemId()){
                    case 0:
                        data.put("frequency", "daily");
                        break;
                    case 1:
                        data.put("frequency", "monthly");
                        break;
                    case 2:
                        data.put("frequency", "semesterly");
                        break;
                    }*/

                    db.collection("users").document(userID).collection(type).document()
                            .set(data)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Log.d(TAG, "DocumentSnapshot successfully updated!");
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error updating document", e);
                                }
                            });
                    if (type.equals("expense")) {
                        Toast.makeText(AddDataActivity.this, getString(R.string.add_expense_data_message), Toast.LENGTH_LONG).show();
                    } else if (type.equals("income")) {
                        Toast.makeText(AddDataActivity.this, getString(R.string.add_income_data_message), Toast.LENGTH_LONG).show();
                    }

                    finish();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}