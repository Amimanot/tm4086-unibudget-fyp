package com.ftsm.tm4086.a162021.unibudget;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class AddRandomData {
    String userID;
    FirebaseFirestore db;
    FirebaseAuth auth;
    FirebaseUser user;
    final String TAG = "FireStoreAddData";

    public void addRandomData(String uID){
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        assert user != null;
        userID = uID;

        final Map<String, Object> income1 = new HashMap<>();
        income1.put("value", "1000.00");
        income1.put("notes", "PTPTN");
        income1.put("timestamp", new Timestamp(randomDate()));
        //income1.put("frequency", "monthly");

        final Map<String, Object> income2 = new HashMap<>();
        income2.put("value", "200.00");
        income2.put("notes", "Ibu bapa");
        income2.put("timestamp", new Timestamp(randomDate()));
        //income2.put("frequency", "monthly");

        final Map<String, Object> expense1 = new HashMap<>();
        expense1.put("value", "5.50");
        expense1.put("notes", "Breakfast");
        expense1.put("timestamp", new Timestamp(randomDate()));
        //expense1.put("frequency", "daily");

        final Map<String, Object> expense2 = new HashMap<>();
        expense2.put("value", "8.00");
        expense2.put("notes", "Lunch");
        expense2.put("timestamp", new Timestamp(randomDate()));
        //expense2.put("frequency", "daily");

        addIncomeData(income1);
        addIncomeData(income2);
        addExpenseData(expense1);
        addExpenseData(expense2);
    }

    public Date randomDate(){
        Random random = new Random();
        long milliseconds = 1593532800000L + (Math.abs(random.nextLong()) % ((long) 30 * 24 * 60 * 60 * 1000)); // Creates a random date from July 1, 2020 12:00:00 AM GMT+08:00 to June 30, 2020 12:00:00 AM GMT+08:00
        return new Date(milliseconds);
    }

    public void addIncomeData(Map<String, Object> data){
        db.collection("users").document(userID).collection("income").document()
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

    public void addExpenseData(Map<String, Object> data){
        db.collection("users").document(userID).collection("expense").document()
                .set(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }
}
