package com.ftsm.tm4086.a162021.unibudget;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "EmailPassword";
    private FirebaseAuth mAuth;
    Button button_login, button_register;
    EditText editText_email, editText_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(getString(R.string.action_login));

        mAuth = FirebaseAuth.getInstance();

        editText_email = findViewById(R.id.login_edittext_email);
        editText_password = findViewById(R.id.login_edittext_password);
        button_login = findViewById(R.id.login_button_login);
        button_register = findViewById(R.id.login_button_signup);

        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(editText_email.getText().toString().trim(), editText_password.getText().toString().trim());
            }
        });

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register_intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(register_intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        // Enable FireStore offline persistence
        if (currentUser != null){
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                    .setPersistenceEnabled(true)
                    .build();
            db.setFirestoreSettings(settings);
        }
        gotoMain(currentUser);
    }

    private void login(String email, String password){
        try {
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        gotoMain(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        // Firebase related errors
                        String errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();

                        Log.w(TAG, errorCode);
                        Log.w(TAG, "signInWithEmail:failure", task.getException());

                        switch (errorCode) {
                            case "ERROR_INVALID_EMAIL":
                                editText_email.setError(getString(R.string.login_invalid_email));
                                editText_email.requestFocus();
                                gotoMain(null);
                                break;

                            case "ERROR_USER_NOT_FOUND":
                                Toast.makeText(LoginActivity.this, getString(R.string.login_no_user), Toast.LENGTH_LONG).show();
                                gotoMain(null);
                                break;

                            case "ERROR_WRONG_PASSWORD":
                                editText_password.setError(getString(R.string.login_wrong_password));
                                editText_password.requestFocus();
                                editText_password.setText("");
                                gotoMain(null);
                                break;
                        }
                    }
                }
            });
        }
        catch (Exception e){
            // Show error when entered fields are empty
            Log.w(TAG, e);
            Toast.makeText(LoginActivity.this, getString(R.string.login_empty_field), Toast.LENGTH_LONG).show();
        }
    }

    private void gotoMain(FirebaseUser currentUser) {
        Intent main_intent = new Intent(LoginActivity.this, MainActivity.class);
        if (currentUser != null) {
            startActivity(main_intent);
            finish();
        }
    }
}