package com.ftsm.tm4086.a162021.unibudget;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "EmailPassword";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    Button button_register;
    EditText editText_name, editText_email, editText_password, editText_confirmPassword;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle(getString(R.string.action_register));

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        editText_name = findViewById(R.id.register_edittext_name);
        editText_email = findViewById(R.id.register_edittext_email);
        editText_password = findViewById(R.id.register_edittext_password);
        editText_confirmPassword = findViewById(R.id.register_edittext_confirm_password);
        button_register = findViewById(R.id.register_button_signup);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        button_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(editText_name.getText().toString())){
                    editText_name.setError(getString(R.string.register_name_empty));
                    editText_name.requestFocus();
                }else if (!editText_password.getText().toString().trim().equals(editText_confirmPassword.getText().toString().trim())){
                    editText_confirmPassword.setError(getString(R.string.register_password_different));
                    editText_confirmPassword.requestFocus();
                    editText_confirmPassword.setText("");
                }else{
                    createAccount(editText_email.getText().toString().trim(), editText_password.getText().toString().trim());
                }
            }
        });
    }

    private void createAccount(String email, String password){
        try {
            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if (task.isSuccessful()) {
                        // Register success, return to login activity to make sure user signs-in
                        Log.d(TAG, "createUserWithEmail:success");
                        Toast.makeText(RegisterActivity.this, getString(R.string.account_created), Toast.LENGTH_LONG).show();
                        addUser();
                    } else {
                        // If register fails, display a message to the user.
                        // Firebase related errors
                        String errorCode = ((FirebaseAuthException) Objects.requireNonNull(task.getException())).getErrorCode();

                        Log.w(TAG, errorCode);
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());

                        switch (errorCode) {
                            case "ERROR_INVALID_EMAIL":
                                editText_email.setError(getString(R.string.login_invalid_email));
                                editText_email.requestFocus();
                                break;

                            case "ERROR_WEAK_PASSWORD":
                                editText_password.setError(getString(R.string.register_short_password));
                                editText_password.requestFocus();
                                break;

                            case "ERROR_EMAIL_ALREADY_IN_USE":
                                Toast.makeText(RegisterActivity.this, getString(R.string.register_email_exists), Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                }
            });
        }
        catch (Exception e){
            // When entered fields are empty
            Log.w(TAG, e);
            Toast.makeText(RegisterActivity.this, getString(R.string.login_empty_field), Toast.LENGTH_LONG).show();
        }
    }

    private void addUser(){
        final Map<String, Object> user = new HashMap<>();
        user.put("name", editText_name.getText().toString());
        user.put("dailylimit", 50);
        user.put("monthlylimit", 1500);
        user.put("semesterlimit", 6000);

        userID = mAuth.getUid();

        db.collection("users").document(userID)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "DocumentSnapshot successfully written!");
                        returnLogin();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w(TAG, "Error writing document", e);
                    }
                });
    }

    private void returnLogin() {
        mAuth.signOut(); //log out from app due to firebase automatically logs in user after account creation
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}