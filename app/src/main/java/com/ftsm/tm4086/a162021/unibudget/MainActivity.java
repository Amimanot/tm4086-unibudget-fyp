package com.ftsm.tm4086.a162021.unibudget;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    TextView text_title, text_total_income, text_total_expense, text_daily_limit, text_monthly_limit, text_semester_limit;
    FloatingActionButton fab_add, fab_savings, fab_expenses;
    float income_total, expense_total, daily_limit, monthly_limit, semester_limit;
    DocumentSnapshot document;
    DocumentReference docRef;
    RecyclerView recyclerView_income, recyclerView_expense;
    private static final String TAG = "FireStore";
    ArrayList<Budget> income_list = new ArrayList<>();
    ArrayList<Budget> expense_list = new ArrayList<>();
    //ArrayList<Budget> daily_expense = new ArrayList<>();
    //ArrayList<Budget> monthly_expense = new ArrayList<>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    DecimalFormat df = new DecimalFormat("0.00");

    static String name, userID;
    boolean isRotate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fab_add = findViewById(R.id.main_fab_add);
        fab_savings = findViewById(R.id.main_fab_savings);
        fab_expenses = findViewById(R.id.main_fab_expenses);
        text_title = findViewById(R.id.main_textview_title);
        recyclerView_income = findViewById(R.id.main_recyclerview_income);
        recyclerView_expense = findViewById(R.id.main_recyclerview_expense);
        text_daily_limit = findViewById(R.id.main_textview_message_daily);
        text_monthly_limit = findViewById(R.id.main_textview_message_monthly);
        text_semester_limit = findViewById(R.id.main_textview_message_semester);
        text_total_income = findViewById(R.id.main_textview_income_total);
        text_total_expense = findViewById(R.id.main_textview_expense_total);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        assert user != null;
        userID = user.getUid();

        FABAnimation.init(fab_savings);
        FABAnimation.init(fab_expenses);
        recyclerView_income.setHasFixedSize(true);
        recyclerView_expense.setHasFixedSize(true);

        fab_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isRotate = FABAnimation.rotateFab(v, !isRotate);
                if(isRotate){
                    FABAnimation.showIn(fab_savings);
                    FABAnimation.showIn(fab_expenses);
                }else{
                    FABAnimation.showOut(fab_savings);
                    FABAnimation.showOut(fab_expenses);
                }
            }
        });

        recyclerView_income.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, recyclerView_income ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        /*Intent add_data_intent = new Intent(MainActivity.this, AddDataActivity.class);
                        add_data_intent.putExtra("type", "income");
                        startActivity(add_data_intent);*/
                        // TODO open addDataActivity to edit data
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        AlertDialog.Builder delete_alert_dialog_builder = new AlertDialog.Builder(MainActivity.this);

                        delete_alert_dialog_builder.setMessage(getString(R.string.delete_data) + "?")
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        /*db.collection("users").document(userID).collection("income").document()
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error deleting document", e);
                                                    }
                                                }); */ //TODO implement delete data
                                        onResume();
                                    }
                                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        delete_alert_dialog_builder.show();
                    }
                })
        );

        recyclerView_expense.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, recyclerView_expense ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        /*Intent add_data_intent = new Intent(MainActivity.this, AddDataActivity.class);
                        add_data_intent.putExtra("type", "expense");
                        startActivity(add_data_intent);*/
                        // TODO open addDataActivity to edit data
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        AlertDialog.Builder delete_alert_dialog_builder = new AlertDialog.Builder(MainActivity.this);

                        delete_alert_dialog_builder.setMessage(getString(R.string.delete_data) + "?")
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        /*db.collection("users").document(userID).collection("income").document()
                                                .delete()
                                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                    @Override
                                                    public void onSuccess(Void aVoid) {
                                                        Log.d(TAG, "DocumentSnapshot successfully deleted!");
                                                    }
                                                })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w(TAG, "Error deleting document", e);
                                                    }
                                                }); */ //TODO implement delete data
                                        onResume();
                                    }
                                }).setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });
                        delete_alert_dialog_builder.show();
                    }
                })
        );

        fab_expenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent add_data_intent = new Intent(MainActivity.this, AddDataActivity.class);
                add_data_intent.putExtra("type", "expense");
                startActivity(add_data_intent);
            }
        });

        fab_savings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent add_data_intent = new Intent(MainActivity.this, AddDataActivity.class);
                add_data_intent.putExtra("type", "income");
                startActivity(add_data_intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadVariables();
    }

    public void loadVariables(){
        docRef = db.collection("users").document(userID);

        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    document = task.getResult();
                    assert document != null;
                    if (document.exists()) {
                        name = Objects.requireNonNull(document.get("name")).toString();
                        Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                        text_title.setText(getString(R.string.welcome) + ", " + name);
                        daily_limit = Float.parseFloat(Objects.requireNonNull(document.get("dailylimit")).toString());
                        monthly_limit = Float.parseFloat(Objects.requireNonNull(document.get("monthlylimit")).toString());
                        semester_limit = Float.parseFloat(Objects.requireNonNull(document.get("semesterlimit")).toString());

                        loadArray();
                    } else {
                        Log.d(TAG, "No such document");
                    }
                } else {
                    Log.d(TAG, "get failed with ", task.getException());
                }
            }
        });
    }
    public void loadArray(){

        income_total = 0;
        expense_total = 0;
        income_list.clear();
        expense_list.clear();
        //daily_expense.clear();
        //monthly_expense.clear();

        db.collection("users").document(userID).collection("income").orderBy("timestamp", Query.Direction.DESCENDING).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                income_total = income_total + Float.parseFloat(Objects.requireNonNull(document.get("value")).toString());
                                income_list.add(new Budget(document.getId(),
                                        Objects.requireNonNull(document.get("notes")).toString(),
                                        Float.parseFloat(Objects.requireNonNull(document.get("value")).toString()),
                                        document.getDate("timestamp")));
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection("users").document(userID).collection("expense").orderBy("timestamp", Query.Direction.DESCENDING).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                Log.d(TAG, document.getId() + " => " + document.getData());
                                expense_total = expense_total + Float.parseFloat(Objects.requireNonNull(document.get("value")).toString());
                                expense_list.add(new Budget(document.getId(),
                                        Objects.requireNonNull(document.get("notes")).toString(),
                                        Float.parseFloat(Objects.requireNonNull(document.get("value")).toString()),
                                        document.getDate("timestamp")));
                                loadRecyclerView();
                            }
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });

    }

    public void loadRecyclerView(){
        //Log.d("ARRAYTEST", Arrays.toString(income_list.toArray()));

        BudgetAdapter income_adapter = new BudgetAdapter(income_list);
        BudgetAdapter expense_adapter = new BudgetAdapter(expense_list);
        recyclerView_income.setAdapter(income_adapter);
        recyclerView_expense.setAdapter(expense_adapter);
        recyclerView_income.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView_expense.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        showLimitMessage();
    }

    public void showLimitMessage(){
        float monthly_limit_percent = (expense_total / monthly_limit) * 100;
        float daily_limit_percent = (expense_total / daily_limit) * 100;
        float semester_limit_percent = (expense_total / semester_limit) * 100;

        text_total_income.setText("RM " + df.format(income_total));
        text_total_expense.setText("RM " + df.format(expense_total));
        text_daily_limit.setText(getString(R.string.you_have_used) + " " + df.format(daily_limit_percent) + "% " + getString(R.string.of_daily_limit));
        text_monthly_limit.setText(getString(R.string.you_have_used) + " " + df.format(monthly_limit_percent) + "% " + getString(R.string.of_monthly_limit));
        text_semester_limit.setText(getString(R.string.you_have_used) + " " + df.format(semester_limit_percent) + "% " + getString(R.string.of_semester_limit));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_item_settings) {
            Intent settings_intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(settings_intent);
        }
        return super.onOptionsItemSelected(item);
    }
}