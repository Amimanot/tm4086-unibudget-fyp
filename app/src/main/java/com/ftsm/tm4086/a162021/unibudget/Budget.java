package com.ftsm.tm4086.a162021.unibudget;

import java.util.Date;

public class Budget {

    String budget_ID, note, frequency;
    float value;
    Date timestamp;

    public Budget(String budget_ID, String note, float value, Date timestamp) {
        this.budget_ID = budget_ID;
        this.note = note;
        this.value = value;
        //this.frequency = frequency;
        this.timestamp = timestamp;
    }

    /*public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }*/

    public String getBudget_ID() {
        return budget_ID;
    }

    public void setBudget_ID(String budget_ID) {
        this.budget_ID = budget_ID;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}


